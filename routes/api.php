<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(
    ['prefix' => 'recipes'],
    function () use ($router) {
        $router->get('/', ['uses' => 'RecipeController@index']);
        $router->get('{id}', ['uses' => 'RecipeController@show']);
        $router->group(
            ['middleware' => 'auth'],
            function () use ($router) {
                $router->post('/', ['uses' => 'RecipeController@create']);
                $router->post('{id}/images', ['uses' => 'ImageController@create']);
                $router->delete('{id}/images/{image_id}', ['uses' => 'ImageController@delete']);
                $router->delete('{id}', ['uses' => 'RecipeController@delete']);
                $router->put('{id}', ['uses' => 'RecipeController@update']);
            }
        );
    }
);

$router->group(
    ['prefix' => 'users'],
    function () use ($router) {
        $router->post('register', ['uses' => 'UserController@register']);
        $router->post('login', ['uses' => 'UserController@login']);
        $router->group(
            ['middleware' => 'auth'],
            function () use ($router) {
                $router->post('/', ['uses' => 'RecipeController@create']);
                $router->delete('{id}', ['uses' => 'RecipeController@delete']);
                $router->put('{id}', ['uses' => 'RecipeController@update']);
            }
        );
    }
);
