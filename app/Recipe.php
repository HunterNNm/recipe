<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'ingredients',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    ];

    protected $casts = [
        'ingredients' => 'array',
    ];

    public function images()
    {
        return $this->hasMany(Image::class);
    }
}
