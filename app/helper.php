<?php

/**
 * @param $name
 * @return string
 */
function get_image_path($name): string
{
    return base_path('public/images/'.$name);
}