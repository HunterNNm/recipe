<?php

namespace App\Events;

class DeleteRecipeEvent extends Event
{
    /**
     * @var int
     */
    public $recipeId;

    /**
     * Create a new event instance.
     *
     * @param int $recipeId
     */
    public function __construct(int $recipeId)
    {
        $this->recipeId = $recipeId;
    }
}
