<?php

namespace App\Listeners;

use App\Events\DeleteRecipeEvent;
use App\Image;

class DeleteRecipeListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param DeleteRecipeEvent $event
     * @return void
     */
    public function handle(DeleteRecipeEvent $event)
    {
        $images = Image::where('recipe_id', $event->recipeId)->get();
        foreach ($images as $image) {
            unlink(get_image_path($image->path));
            $image->delete();
        }
    }
}
