<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Image;
use App\Recipe;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ImageController extends Controller
{
    /**
     * @param Request $request
     * @param $id
     * @return JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function create(Request $request, $id): JsonResponse
    {
        $this->authorize('update-recipe', $recipe = Recipe::findOrFail($id));

        $this->validate(
            $request,
            [
                'images.*' => 'image|max:10240',
            ]
        );

        $images = $request->file('images');
        /** @var \Illuminate\Http\UploadedFile $image */
        foreach ($images as $image) {
            $imageModel = new Image();
            $imageModel->path = $name = $image->hashName();
            move_uploaded_file($image->getPathname(), get_image_path($name));
            $imageModel->save();
            $recipe->images()->save($imageModel);
        }

        return response()->json($recipe->load('images'));
    }

    /**
     * @param Request $request
     * @param int $id
     * @param int $recipeId
     * @return JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function delete(Request $request, int $id, int $recipeId): JsonResponse
    {
        $this->authorize('update-recipe', $recipe = Recipe::findOrFail($id));
        $image = Image::find($recipeId);
        unlink(get_image_path($image->path));
        $image->delete();

        return response()->json(['message' => 'success']);
    }
}
