<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function register(Request $request): JsonResponse
    {
        $data = $this->validate(
            $request,
            [
                'name' => 'required',
                'email' => 'required|email|unique:users',
                'password' => 'required|string|min:8|max:32',
            ]
        );
        $data['api_token'] = str_random(32);
        $data['password'] = app('hash')->make($data['password']);
        $user = User::create($data);

        return response()->json($user, 201);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request): JsonResponse
    {
        $data = $this->validate(
            $request,
            [
                'email' => 'required|email|exists:users,email',
                'password' => 'required|string|min:8|max:32',
            ]
        );
        $user = User::where('email', $data['email'])->firstOrFail();
        if (Hash::check($request->get('password'), $user->password)) {
            $user->api_token = str_random(32);
            $user->save();

            return response()->json($user);
        }

        return response()->json(['status' => 'fail'], 401);
    }
}
