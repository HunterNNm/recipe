<?php

namespace App\Http\Controllers\Api;

use App\Events\DeleteRecipeEvent;
use App\Http\Controllers\Controller;
use App\Recipe;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RecipeController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()->json(Recipe::paginate(5));
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        return response()->json(Recipe::findOrFail($id));
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function create(Request $request): JsonResponse
    {
        $data = $this->validate(
            $request,
            [
                'name' => 'required|string|min:3|max:255',
                'ingredients' => 'required',
            ]
        );

        $recipe = Auth::user()->recipes()->create($data);

        return response()->json($recipe);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, int $id): JsonResponse
    {
        $recipe = Recipe::findOrFail($id);
        $this->authorize('update-recipe', $recipe);
        $data = $this->validate(
            $request,
            [
                'name' => 'string|min:3|max:255',
                'ingredients' => '',
            ]
        );
        $recipe->update($data);

        return response()->json($recipe);
    }

    /**
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function delete(Request $request, int $id): JsonResponse
    {
        $this->authorize('delete-recipe', $recipe = Recipe::findOrFail($id));
        $recipeId = $recipe->id;
        $recipe->delete();
        event(new DeleteRecipeEvent($recipeId));

        return response()->json(['message' => 'success']);
    }
}
